﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escape.Studies.Model
{
    public interface IEventRepository
    {
        IEnumerable<Event> GetAll();
        Event Get(int id);
        Event Add(Event item);

    }
}
