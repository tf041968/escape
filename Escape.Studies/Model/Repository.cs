﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Escape.Studies.Model
{
    public class Repository : IEventRepository
    {

        private List<Event> events = new List<Event>();
        private int _nextId = 1;

        public IEnumerable<Event> GetAll()
        {
            return events;
        }

        public Event Get(int id)
        {
            return events.Find(p => p.Id == id);
        }

        
        public Event Add(Event item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }

            item.Id = _nextId++;
            events.Add(item);
            return item;
        }

        //public void Remove(int id)
        //{
        //    products.RemoveAll(p => p.Id == id);
        //}

        //public bool Update(Product item)
        //{
        //    if (item == null)
        //    {
        //        throw new ArgumentNullException("item");
        //    }
        //    int index = products.FindIndex(p => p.Id == item.Id);
        //    if (index == -1)
        //    {
        //        return false;
        //    }
        //    products.RemoveAt(index);
        //    products.Add(item);
        //    return true;
        //}


    }
}
