﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.SelfHost;

namespace Escape.Studies
{
	class Server
	{
		private readonly HttpSelfHostServer _server;
		
		/// <summary>
		/// Server config
		/// </summary>
		public Server()
		{
			var cfg = new HttpSelfHostConfiguration("http://localhost:8890");
			cfg.Routes.MapHttpRoute(
				name: "default",
				routeTemplate: "{controller}/{id}",
				defaults: new
				{
					controller = "Import",
					id = RouteParameter.Optional
				});

			cfg.Formatters.Clear();
			cfg.Formatters.Add(new XmlMediaTypeFormatter { Indent = true });
			cfg.Formatters.Add(new JsonMediaTypeFormatter());
			_server = new HttpSelfHostServer(cfg);
		}

		/// <summary>
		/// Startar server
		/// </summary>
		public void Start()
		{
			_server.OpenAsync().Wait();
			Console.WriteLine("Server startad. Väntar...");
		}

		/// <summary>
		/// Avslutar server
		/// </summary>
		public void Dispose()
		{
			if (_server != null) _server.Dispose();
		}
	}
}
