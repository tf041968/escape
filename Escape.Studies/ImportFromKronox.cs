﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Web.Http;
using System.Web.Http.SelfHost;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using Escape.Data;

namespace Escape.Studies
{
	public class ImportFromKronox
	{
		private HttpClient client;
		private List<AppointmentRequest> appreqs;
		public ImportFromKronox()
		{
			client = new HttpClient();
			client.BaseAddress = new Uri("http://localhost:8888/appointment?debug=true");
			ImportAppointment();
			appreqs = new List<AppointmentRequest>();
		}
		private void ImportAppointment()
		{
			var req = new HttpRequestMessage(HttpMethod.Get, client.BaseAddress);
			req.Headers.Add("Accept", "application/xml");
			Console.WriteLine(req.ToString());
			using (var resp = client.GetAsync(client.BaseAddress).Result)
			{

				Console.WriteLine(resp);
				if (!resp.IsSuccessStatusCode)
				{
					var error = resp.Content.ReadAsAsync<HttpError>().Result;
					foreach (var kvp in error.Values)
					{
						Console.WriteLine(kvp.ToString());
					}
				}
				else
				{
					var content = resp.Content.ReadAsAsync<AppointmentRequest>().Result;
					Console.WriteLine(content.Participants.ElementAt(0).Name);
					appreqs.Add(content);
				}

				Console.WriteLine("Tagit emot från server");
			}
			Console.ReadLine();
		}
		
	}
}