﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Escape.Data;

namespace Escape.Studies
{
	class Program
	{
		static void Main(string[] args)
		{			
			Server server = new Server();
			Console.WriteLine("Startar servern...");
			server.Start();
			Console.WriteLine("Tryck Enter för att avsluta");
			Console.ReadLine();
			server.Dispose();
		}
	}
}
