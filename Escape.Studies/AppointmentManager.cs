﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Escape.Data;
using System.Net;

namespace Escape.Studies
{
	/// <summary>
	/// Singleton class
	/// </summary>
	public class AppointmentManager
	{
		List<AppointmentRequest> appointments;
		ExportToManager export;

		private static AppointmentManager instance;

		private AppointmentManager()
		{
			appointments = new List<AppointmentRequest>();
			export = new ExportToManager();
		}

		/// <summary>
		/// Singleton konstruktor
		/// </summary>
		public static AppointmentManager Instance
		{
			get 
			{
				if (instance == null)
				{
					instance = new AppointmentManager();
				}
				return instance;
			}
		}

		/// <summary>
		/// Tar emot request från från kronox och skickar den vidare för export. 
		/// Lägger till appointmentrequesten till en lista.  
		/// </summary>
		/// <param name="req">Appointmentrequest</param>
		public void AppointmentRequest(AppointmentRequest req)
		{
			appointments.Add(req);
			ExportStudies(req);
			if (req.Type != AppointmentType.SelfStudies)
			{
				GenerateStudies(req);
			}
		}

		/// <summary>
		/// Genrerar selfstudiesobjekt och skickar dessa vidare för export. 
		/// </summary>
		/// <param name="req">AppointmentRequest</param>
		private void GenerateStudies(AppointmentRequest req)
		{
			int studylength;
			int numberOfStudyAppointments;

			//Bestämmer hur mycket man behöver studera innan varje studietyp.
			switch (req.Type)
			{
				case AppointmentType.Exam:
					studylength = 8;
					numberOfStudyAppointments = 2;
					break;
				case AppointmentType.Lab:
					studylength = 1;
					numberOfStudyAppointments = 1;
					break;
				case AppointmentType.Lecture:
					studylength = 2;
					numberOfStudyAppointments = 1;
					break;
				case AppointmentType.Seminar:
					studylength = 4;
					numberOfStudyAppointments = 1;
					break;
				default:
					studylength = 2;
					numberOfStudyAppointments = 1;
					break;
			}

			//Skapar SelfStudy-objekt
			for (int i = 0; i < numberOfStudyAppointments; i++)
			{
				TimeSpan ts = new TimeSpan((studylength*(i+1))+1, 0, 0);
				SelfStudies studyRequest = new SelfStudies();
				studyRequest.Parent = req;
				studyRequest.Identifier = studyRequest.Parent.Identifier + i.ToString();
				studyRequest.Description = "Studera inför " + req.Type + " " + req.Description;
				studyRequest.StartTime = req.StartTime.Subtract(ts);
				studyRequest.EndTime = studyRequest.StartTime.AddHours((double)studylength);
				studyRequest.Type = AppointmentType.SelfStudies;
				studyRequest.Priority = 5;
				appointments.Add(studyRequest);
				ExportStudies(studyRequest);
			}
		}

		/// <summary>
		/// Exporterar Appointmentrequests samt selfstudies
		/// </summary>
		/// <param name="appreq">AppointmentRequest</param>
		private void ExportStudies(AppointmentRequest appreq)
		{
			Console.WriteLine(appreq.Identifier + ", " + appreq.Description + " exporteras till manager.");
			export.AddEvent(appreq);
		}

		/// <summary>
		/// Tar emot svar gällande appointmentreq. 
		/// </summary>
		/// <param name="message">Appointmentmessage</param>
		/// <returns>HttpStatusCode</returns>
		public HttpStatusCode AppointmentResponse(AppointmentMessage message) {
			AppointmentRequest request;
			SelfStudies study;
			HttpStatusCode statusCode = HttpStatusCode.OK;
			bool appointmentFound = false;
			for (int i = 0; i < appointments.Count; i++)
			{
				request = appointments.ElementAt(i);
				if (request.Identifier == message.Appointment.Identifier)
				{
					appointmentFound = true;
					Console.Write(request.Identifier + " ");
					
					switch(message.State)
					{
						case State.Accepted:
							statusCode = HttpStatusCode.Accepted;
							Console.WriteLine("Accepted. Mötet är bokat.");
							appointments.RemoveAt(i);
							break;

						case State.Rejected:
							statusCode = HttpStatusCode.Accepted;
							Console.WriteLine("Rejected. Ett fel uppstod. ");
							appointments.RemoveAt(i);
							break;

						case State.Conflict:
							Console.WriteLine("Conflict. Vi försöker omboka.");
							if (request.Type == AppointmentType.SelfStudies)
							{
								study = (SelfStudies)request;
								if (message.Appointment.EndTime < study.Parent.StartTime && message.Appointment.StartTime > DateTime.Now)
								{
									//Om nya tiden slutar innan lektionen, som studietiden gäller, börjar.
									Console.WriteLine("Konflikten är lösbar. Den ombokades.");
									//string response = Console.ReadLine();
									//if (response == "y")
									//{
									appointments.RemoveAt(i);
									appointments.Add(message.Appointment);
									ExportStudies(message.Appointment);
									Console.WriteLine("Mötet är ändrat");
									//}
									//else
									//{
									//	appointments.RemoveAt(i);
									//	Console.WriteLine("Mötet är borttaget");
									//}
									statusCode = HttpStatusCode.Accepted;
								}
								else
								{
									//Tiden är felaktig. 
									appointments.RemoveAt(i);
									Console.WriteLine("Nya studietiden överlappar med lektionen.");
									statusCode = HttpStatusCode.Conflict;
								}
							}
							else
							{
								appointments.RemoveAt(i);
								statusCode = HttpStatusCode.Conflict;
								Console.WriteLine("Konflikt med en mötestyp som inte går att omboka.");
							}

							break;
						default:
							Console.WriteLine("Statuskoden är inte hanterbar.");
							break;
					}
					break;
				}
			}
			if (!appointmentFound)
			{
				Console.WriteLine("Appointment not found :(");
				statusCode = HttpStatusCode.NotFound;
			}
			return statusCode;
		}
	}
}
