﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using Escape.Data;
using System.Web.Http;

namespace Escape.Studies
{
	public class ExportToManager
	{
	   static HttpClient client;
	   
		public ExportToManager()
		{
			client = new HttpClient();
		}

		/// <summary>
		/// Skickar vidare en appointmentreq till Escape.Studies.Manager
		/// </summary>
		/// <param name="appointment">AppointmentRequest</param>
		public void AddEvent(AppointmentRequest appointment)
		{
			//string ip = "http://localhost:8888/appointment";
			string ip = "http://10.5.5.212:8888/appointment";
			var req = new HttpRequestMessage(HttpMethod.Post, ip);
			var content = appointment;
			req.Content = new ObjectContent<AppointmentRequest>(content, new XmlMediaTypeFormatter());
			req.Content.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/xml");

			using (var resp = client.SendAsync(req).Result)
			{
				if (!resp.IsSuccessStatusCode)
				{
					var error = resp.Content.ReadAsAsync<HttpError>().Result;
					foreach (var kvp in error.Values)
					{
						Console.WriteLine(kvp.ToString());
					}
				}
				Console.WriteLine("Skickat till server");
			}	
		}

		/// <summary>
		/// Skickar kurskod till kronoxgruppen
		/// </summary>
		/// <param name="courseID">Kurskod som sträng</param>
		public void AddCourse(string courseID)
		{
			string ip = "http://localhost:8890";
			var req = new HttpRequestMessage(HttpMethod.Post, ip);
			var content = courseID;
			req.Content = new ObjectContent<string>(content, new XmlMediaTypeFormatter());
			req.Content.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/xml");

			using (var resp = client.SendAsync(req).Result)
			{
				if (!resp.IsSuccessStatusCode)
				{
					var error = resp.Content.ReadAsAsync<HttpError>().Result;
					foreach (var kvp in error.Values)
					{
						Console.WriteLine(kvp.ToString());
					}
				}
				Console.WriteLine("Skickat till server");
			}
		}
	}
}
