﻿using System.Web.Http;

namespace Escape.Studies.Controllers
{
    public class PutController : ApiController
    {
        private string name = "def";
        private string adress = "def";

        public void Post(string name, string adress)
        {
            this.name = name;
            this.adress = adress;
        }

        public string Get()
        {
            return name + " bor på " + adress;
        }
    }
}
