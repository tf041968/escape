﻿using Escape.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Escape.Studies.Controllers
{
	public class ResponseController : ApiController
	{
		/// <summary>
		/// Tar emot post-message. 
		/// </summary>
		/// <param name="message">AppointmentMessage</param>
		/// <returns>HttpResponseMessage</returns>
		public HttpResponseMessage PostResponse(AppointmentMessage message)
		{
			HttpStatusCode statusCode = AppointmentManager.Instance.AppointmentResponse(message);
			var resp = new HttpResponseMessage(statusCode);
			var ub = new UriBuilder(ControllerContext.Request.RequestUri);

			resp.Headers.Add("Location", ub.Uri.ToString());
			return resp;
		}
	}
}
