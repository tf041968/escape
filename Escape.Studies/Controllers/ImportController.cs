﻿using System.Web.Http;
using System.Net;
using System.Net.Http;
using System;
using System.Collections.Generic;
using Escape.Studies.Model;
using Escape.Data;
using Escape.Studies;

namespace Escape.Studies.Controllers
{
	public class ImportController : ApiController
	{
		/// <summary>
		/// Tar emot appointment.req från Kronox
		/// </summary>
		/// <param name="request">AppointmentRequest</param>
		/// <returns></returns>
		public HttpResponseMessage PostRequest(AppointmentRequest request)
		{
			AppointmentManager.Instance.AppointmentRequest(request);
			var resp = new HttpResponseMessage(HttpStatusCode.Created);
			var ub = new UriBuilder(ControllerContext.Request.RequestUri);

			resp.Headers.Add("Location", ub.Uri.ToString());
			return resp;
		}
	}
}
