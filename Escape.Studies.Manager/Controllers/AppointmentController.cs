﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Escape.Data;
using System.Linq;

namespace Escape.Studies.Manager.Controllers {
	public class AppointmentController : ApiController {
		private readonly IAppointmentRepository _repository;

		public AppointmentController(IAppointmentRepository repo) {
			_repository = repo;
		}

		public HttpResponseMessage Post(AppointmentRequest appointment) {
			if (appointment == null) {
				return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid or missing data in body.");
			}

			var msg = _repository.GetItem(appointment);
			if (msg != null) {
				if (msg.Appointment.Equals(appointment) && msg.State == State.Conflict) {
					_repository.Remove(msg);
					msg = new AppointmentMessage(appointment);
					_repository.Create(msg);
				}
			} else {
				msg = new AppointmentMessage(appointment);
				_repository.Create(msg);
			}

			var resp = new HttpResponseMessage(HttpStatusCode.Created);
			var ub = new UriBuilder(Request.RequestUri);
			ub.Path = ub.Path.TrimEnd('/') + '/' + msg.Appointment.Identifier;

			resp.Headers.Add("Location", ub.Uri.ToString());
			return resp;
		}

		public HttpResponseMessage GetById(string id) {
			var app = _repository.AppointmentByResourceId(id) as AppointmentMessage;
			if (app == null) return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Invalid id");

			return Request.CreateResponse(HttpStatusCode.OK, app);
		}

#if DEBUG
		public IEnumerable<AppointmentMessage> GetAll() {
			try {
				return _repository.Cast<AppointmentMessage>();
			} catch (InvalidCastException ex) {
				Console.WriteLine("Cast failed: {0}", ex.Message);
				return Enumerable.Empty<AppointmentMessage>();
			}
		}

		public HttpResponseMessage GetTestData(bool debug) {
			if (debug) {
				var appointment = new AppointmentRequest {
					Description  = "This is a description",
					Location     = "Location: Somewhere in the universe",
					Participants = new List<Participant> {
						new Participant { Name = "Pelle", Username = "MP11954" },
						new Participant { Name = "Kalle", Username = "MP10213" }
					},
					Priority   = 10,
					Sender     = Sender.Studies,
					StartTime  = DateTime.Now,
					User       = "MP11954",
					Identifier = Guid.NewGuid().ToString("N"),
					Type       = AppointmentType.SelfStudies
				};

				return Request.CreateResponse(HttpStatusCode.OK, appointment);
			}

			return Request.CreateResponse(HttpStatusCode.OK, GetAll());
		}
#endif
	}
}
