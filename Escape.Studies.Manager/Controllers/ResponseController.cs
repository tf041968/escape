﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text;
using System.Threading.Tasks;

using Escape.Data;

namespace Escape.Studies.Manager.Controllers {
	public class ResponseController : ApiController {
		private readonly IAppointmentRepository _repository;
		public ResponseController(IAppointmentRepository repository) {
			_repository = repository;
		}

		public HttpResponseMessage Post(AppointmentMessage msg) {
			if (msg == null
				|| msg.Appointment == null
				|| String.IsNullOrEmpty(msg.Appointment.Identifier)) {
				return Request.CreateResponse(HttpStatusCode.BadRequest, "Require appointment or appointment properties missing");
			}

			var cur = _repository.GetItem(msg.Appointment);
			if (cur == null) {
				return Request.CreateResponse(HttpStatusCode.NotFound, "Response to unknown appointment");
			}

			if (msg.State == cur.State) return Request.CreateResponse(HttpStatusCode.NotModified);

			bool handled = false;
			switch (msg.State) {
			case State.Accepted:
				handled = _repository.Accept(msg);
				break;
			case State.Conflict:
				handled = _repository.Conflict(msg);
				break;
			case State.Rejected:
				handled = _repository.Reject(msg);
				break;
			default:
				return Request.CreateResponse(HttpStatusCode.BadRequest, "Message state is invalid");
			}

			return Request.CreateResponse(handled ? HttpStatusCode.OK : HttpStatusCode.InternalServerError);
		}
	}
}
