﻿using System;
using System.IO;
using System.Net;
using System.Xml;
using Newtonsoft.Json;
using System.Collections.Generic;
using Escape.Data;

namespace Escape.Kronox
{
	/// <summary>
	/// SIMULERAR KRONOXGRUPPEN
	/// </summary>
	class GetCourseFromKronox
	{
		AppointmentRequest appointmentRequest;
		List<AppointmentRequest> listOfAppointmentRequests;
		private string courseCode;
		private string kronoxURL;

		/// <summary>
		/// Hanterar datan som returneras från kronox när man gör anrop för att hämta hela kurskoden. s
		/// </summary>
		private class Course
		{
			public string value { get; set; }
			public string label { get; set; }
		}

		/// <summary>
		/// Konstruktor
		/// URL för att få fram fullständig kurskod. 
		/// </summary>
		public GetCourseFromKronox()
		{
			kronoxURL = "http://kronox.mah.se/ajax/ajax_autocompleteResurser.jsp?typ=kurs&term=";
		}

		/// <summary>
		/// Skapar en lista och sedan en kedja av metoder för att fylla listan med kursinformation
		/// </summary>
		/// <param name="userCourse">Användarinput som ska vara en kurskod</param>
		/// <returns>En lista som innehåller alla det lektioner/föreläsningar som finns i kursen. </returns>
		public List<AppointmentRequest> GetAppointmentRequests(string userCourse)
		{
			listOfAppointmentRequests = new List<AppointmentRequest>();
			kronoxURL = kronoxURL + userCourse;
			bool url = GetUrlContent(kronoxURL);
			checkUrlContent(url);
			return listOfAppointmentRequests;
		}

		/// <summary>
		/// Gör anrop till kronox och hämtar ut fullständig kurskod baserad på den input användaren angett
		/// </summary>
		/// <param name="kronoxURL">Fullständig url till kronox</param>
		/// <returns></returns>
		private bool GetUrlContent(string kronoxURL)
		{
			WebResponse objResponse;
			HttpWebRequest request = null;
			request = HttpWebRequest.Create(kronoxURL) as HttpWebRequest;
			request.Method = "GET";
			request.ContentType = "application/json";
			objResponse = request.GetResponse();

			//Använder streamreader för att läsa den hämtade datan. 
			using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
			{
				string line = sr.ReadLine();    //Läser rad
				List<Course> courseObj = JsonConvert.DeserializeObject<List<Course>>(line); //Skapar obj

				if (courseObj.Count != 0) //Om det finns ngt obj hämtat. 
				{
					courseCode = courseObj[0].value;        //Hämtar kurskoden
					return true;
				}
				else  //Om det inte finns ngt obj. 
				{
					Console.WriteLine("Kursen kunde inte hittas");
					return false;
				}
			}
		}


		/// <summary> 
		/// Kollar om angiven url=true. 
		/// Om den är det så körs en parse-metod annars får användaren mata in en korrekt kurskod. 
		/// </summary>
		/// <param name="url">Fullständig url till angiven kurs-sida</param>
		private void checkUrlContent(bool url)
		{
			if (url)
			{
				ParseXML();
			}
			else
			{
				string userCourse = "";
				while (userCourse.Length != 6)
				{
					Console.Write("Ange kurs enligt format AB123CD: ");
					userCourse = Console.ReadLine();
				}
				kronoxURL = "http://kronox.mah.se/ajax/ajax_autocompleteResurser.jsp?typ=kurs&term=" + userCourse;
				checkUrlContent(GetUrlContent(kronoxURL));
			}
		}

		/// <summary>
		/// Anropar kronox för att hämta schemat till angiven kurskod. Parsar datan och sparar den som AppointmentRequests. 
		/// Sparar varje AppReq i en lista
		/// </summary>
		private void ParseXML()
		{
			appointmentRequest = new AppointmentRequest();
			XmlDocument xmlDoc = new XmlDocument(); //Dokumenthållare för XML
			string url = "http://schema.mah.se/setup/jsp/SchemaXML.jsp?startDatum=idag&intervallTyp=m&intervallAntal=6&sokMedAND=false&sprak=SV&resurser=k." + courseCode;
			xmlDoc.Load(url); //Laddar URL till dokumenthållare. 

			//Hämtar element "schemaPost" och sparar des noder i en lista. 
			XmlNodeList listOfNodes = xmlDoc.DocumentElement.SelectNodes("schemaPost");

			//Loopar listan med noder och plockar ut de noder/attribut som är nödvändiga för applikationen. 
			//Sparar alla dessa som ett AppointmentRequest-objekt.  
			foreach (XmlNode node in listOfNodes)
			{
				appointmentRequest = new AppointmentRequest();
				appointmentRequest.Identifier = node.SelectSingleNode("bokningsId").InnerXml;
				appointmentRequest.NumberOfChanges = Convert.ToInt32(node.SelectSingleNode("versionsIndex").InnerXml);
				appointmentRequest.StartTime = DateTime.ParseExact(node.SelectSingleNode("bokadeDatum").Attributes["startDatumTid"].Value, "yyyy-MM-dd HH:mm:ss",
												 null);
				appointmentRequest.EndTime = DateTime.ParseExact(node.SelectSingleNode("bokadeDatum").Attributes["slutDatumTid"].Value, "yyyy-MM-dd HH:mm:ss",
												 null);
				appointmentRequest.LastUpdate = DateTime.ParseExact(node.SelectSingleNode("senastAndradDatum").InnerXml, "yyyy-MM-dd HH:mm:ss",
												 null);
				appointmentRequest.Description = node.SelectSingleNode("moment").InnerText;
				switch (appointmentRequest.Description)
				{
					case "Tentamen":
					case "Omtentamen":
					case "Redovisning":
						appointmentRequest.Type = AppointmentType.Exam;
						appointmentRequest.Priority = 10;
						break;
					case "Seminarium":
						appointmentRequest.Type = AppointmentType.Seminar;
						appointmentRequest.Priority = 7;
						break;
					default:
						appointmentRequest.Type = AppointmentType.Lecture;
						appointmentRequest.Priority = 1;
						break;
				}

				//Loopar barn-noder i en nod, för att plocka ut lokal. 
				for (int i = 0; i < node.SelectSingleNode("resursTrad").ChildNodes.Count; i++)
				{
					XmlNode selectedNode = node.SelectSingleNode("resursTrad").ChildNodes[i];
					if (selectedNode.Attributes["resursTypId"].Value == "RESURSER_LOKALER")
						appointmentRequest.Location = selectedNode.SelectSingleNode("resursId").InnerText;
				}
				listOfAppointmentRequests.Add(appointmentRequest);

				Console.WriteLine("Bokningsnr " + appointmentRequest.Identifier);
				Console.WriteLine("Beskrivning " + appointmentRequest.Description);
				Console.WriteLine("Antal ändringar " + appointmentRequest.NumberOfChanges);
				Console.WriteLine("Starttid " + appointmentRequest.StartTime);
				Console.WriteLine("Sluttid " + appointmentRequest.EndTime);
				Console.WriteLine("Senast ändrad " + appointmentRequest.LastUpdate);
				Console.WriteLine("Prioritet: " + appointmentRequest.Priority);
				Console.WriteLine("i Sal  " + appointmentRequest.Location);
			}
		}
	}
}
