﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Escape.Data;

namespace Escape.Kronox
{
	/// <summary>
	/// SIMULERAR KRONOXGRUPPEN
	/// </summary>
	public class Program
	{
		static void Main(string[] args)
		{
			GetCourseFromKronox kronox = new GetCourseFromKronox();
			Start start = new Start();
			start.Init();
			Console.ReadLine();
		}

		public class Start
		{
			public void Init()
			{
				while (true)
				{
					string userCourse = "";
					while (userCourse.Length != 6)
					{
						Console.Write("Ange kurs enligt format AB123C: ");
						userCourse = Console.ReadLine();
					}

					GetCourseFromKronox kronox = new GetCourseFromKronox();
					ExportAppointmentRequest export = new ExportAppointmentRequest();
					List<AppointmentRequest> lista = kronox.GetAppointmentRequests(userCourse);
					export.ExportAppointment(lista);
					Console.WriteLine("Det finns " + lista.Count + " stycken möten i listan");
				}
			}
		}
	}
}
