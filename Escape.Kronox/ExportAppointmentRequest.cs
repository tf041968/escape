﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Formatting;
using System.Web.Http;
using Escape.Data;

namespace Escape.Kronox
{
	/// <summary>
	/// SIMULERAR KRONOXGRUPPEN
	/// </summary>
    class ExportAppointmentRequest
    {

        static HttpClient client;

        public ExportAppointmentRequest()
        {
            client = new HttpClient();
            //client.BaseAddress = new Uri("http://localhost:8889/api/");
        }

        public void ExportAppointment(List<AppointmentRequest> listOfAppointments)
        {
            foreach (AppointmentRequest request in listOfAppointments)
            {
                PostAppointment(request);
            }
        }

        private void PostAppointment(AppointmentRequest appointment)
        {
            var req = new HttpRequestMessage(HttpMethod.Post, "http://localhost:8890" + "/Import");
            var content = appointment;
            req.Content = new ObjectContent<AppointmentRequest>(content, new XmlMediaTypeFormatter());
            req.Content.Headers.ContentType = new MediaTypeWithQualityHeaderValue("application/xml");

            using (var resp = client.SendAsync(req).Result)
            {
                if (!resp.IsSuccessStatusCode)
                {
                    //var error = resp.Content.ReadAsAsync<HttpError>().Result;
                    //foreach (var kvp in error.Values)
                    //{
                    //    Console.WriteLine(kvp.ToString());
                    //}
                }
                Console.WriteLine("Skickat till server");
            }
        }
    }
}
