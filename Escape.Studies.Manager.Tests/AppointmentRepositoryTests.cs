﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Escape.Data;
using NSubstitute;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace Escape.Studies.Manager.Tests
{
	public class AppointmentRepositoryTests {
		private IFixture _fixture = new Fixture();
		private IDispatcher<IAppointmentMessage> _observer;

		[TestFixtureSetUp]
		public void TextFixtureSetup() {
			_observer = Substitute.For<IDispatcher<IAppointmentMessage>>();
		}

		[Test]
		public void Add_AppointmentToRepository_Calls_OnNext() {
			var repo = new AppointmentRepository();
			var appointment = _fixture.CreateAnonymous<AppointmentMessage>();
			// appointment.State flag must be set on _observer.StateFilter
			appointment.State = State.Created;
			_observer.StateFilter.Returns(State.Created);

			using (repo.Subscribe(_observer)) {
				repo.Create(appointment);
				_observer.Received().OnNext(Arg.Is(appointment));
			}
		}

		[Test]
		public void Add_AppointmentToRepository_AfterUnsubscribing_DoesNotCall_OnNext() {
			var repo = new AppointmentRepository();
			var appointment = _fixture.CreateAnonymous<AppointmentMessage>();
			// appointment.State flag must be set on _observer.StateFilter
			appointment.State = State.Created;
			_observer.StateFilter.Returns(State.Created);

			using (repo.Subscribe(_observer)) {
			}
			repo.Create(appointment);
			_observer.DidNotReceive().OnNext(Arg.Any<IAppointmentMessage>());
		}

		[Test]
		public void Conflict_Calls_OnNext() {
			var repo = new AppointmentRepository();
			var appointment = _fixture.CreateAnonymous<AppointmentMessage>();
			// appointment.State flag must be set on _observer.StateFilter
			appointment.State = State.Conflict;
			_observer.StateFilter.Returns(State.Conflict);

			using (repo.Subscribe(_observer)) {
				repo.Create(appointment);
				_observer.Received().OnNext(Arg.Is(appointment));
			}
		}

		[Test]
		public void Reject_RemovesAppointmentFromRepository() {
			var repo = new AppointmentRepository();
			var stub = _fixture.CreateAnonymous<AppointmentMessage>();
			repo.Create(stub);

			var resp = new AppointmentMessage(stub.Appointment);
			resp.State = State.Rejected;

			repo.Reject(resp);
			
			Assert.AreEqual(0, repo.Count);
		}

		[Test]
		public void Accept_RemovesAppointmentFromRepository() {
			var repo = new AppointmentRepository();
			var stub = _fixture.CreateAnonymous<AppointmentMessage>();
			repo.Create(stub);

			var resp = new AppointmentMessage(stub.Appointment);
			resp.State = State.Accepted;

			repo.Reject(resp);
			
			Assert.AreEqual(0, repo.Count);
		}
	}
}
