﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;

using Autofac;
using Autofac.Integration.WebApi;
using Escape.Data;
using NSubstitute;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace Escape.Studies.Manager.Tests {
	public class ResponseControllerTests : IDisposable {
		private HttpServer _server;
		private HttpClient _client;
		private HttpConfiguration _config;
		private IFixture _fixture;
		private IAppointmentRepository _repository;
		private const string BaseUri = @"http://random-non-existant-domain.net";
		private const string ResourceUri = BaseUri + "/Response";

		[TestFixtureSetUp]
		public void FixtureSetup() {
			_config = new Manager().Config;
			_repository = new AppointmentRepository();

			// Set up IoC
			var builder = new ContainerBuilder();
			builder.RegisterApiControllers(typeof(Controllers.AppointmentController).Assembly);
			builder.Register(ctx => _repository)
				.As<IAppointmentRepository>()
				.SingleInstance();

			_config.DependencyResolver = new AutofacWebApiDependencyResolver(builder.Build());
			_config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
			_server = new HttpServer(_config);
		}

		[TestFixtureTearDown]
		public void Dispose() {
			if (_server != null) _server.Dispose();
		}

		[SetUp]
		public void Setup() {
			_client = new HttpClient(_server);
			_fixture = new Fixture();
		}

		[Test]
		public void Post_UnknownAppointmentMessage_Returns_HttpStatusNotFound() {
			var stub = _fixture.CreateAnonymous<AppointmentMessage>();

			var resp = _client.PostAsXmlAsync(ResourceUri, stub).Result;
			
			Assert.AreEqual(HttpStatusCode.NotFound, resp.StatusCode);
		}

		[Test]
		public void Post_UnchangedState_Returns_HttpStatusNotModified() {
			var stub = _fixture.CreateAnonymous<AppointmentMessage>();

			_repository.Create(stub);
			var resp = _client.PostAsXmlAsync(ResourceUri, stub).Result;
			
			Assert.AreEqual(HttpStatusCode.NotModified, resp.StatusCode);
		}

		[Test]
		public void Post_AcceptedState_Calls_AcceptedOnRepository() {
			// Order matter; MessageStub() sets up AutoFixture customization
			var stub = MessageStub();
			var repo = RepoMock();

			var controller = new Controllers.ResponseController(repo);
			SetupController(controller);
			stub.State = State.Accepted;

			controller.Post(stub);

			repo.Received().Accept(Arg.Is(stub));
		}

		[Test]
		public void Post_ConflictState_Calls_ConflictOnRepository() {
			// Order matter; MessageStub() sets up AutoFixture customization
			var stub = MessageStub();
			var repo = RepoMock();

			var controller = new Controllers.ResponseController(repo);
			SetupController(controller);
			stub.State = State.Conflict;

			controller.Post(stub);

			repo.Received().Conflict(Arg.Is(stub));
		}
		
		[Test]
		public void Post_RejectedState_Calls_RejectOnRepository() {
			// Order matter; MessageStub() sets up AutoFixture customization
			var stub = MessageStub();
			var repo = RepoMock();

			var controller = new Controllers.ResponseController(repo);
			SetupController(controller);
			stub.State = State.Rejected;

			controller.Post(stub);

			repo.Received().Reject(Arg.Is(stub));
		}

		private AppointmentMessage MessageStub() {
			var id = _fixture.CreateAnonymous<string>();
			var state = State.Pending;
			_fixture.Customize<AppointmentMessage>(e => e.Do(msg => {
				msg.Appointment.Identifier = id;
				msg.State = state;
			}));

			return _fixture.CreateAnonymous<AppointmentMessage>();
		}

		private IAppointmentRepository RepoMock() {
			var repo = Substitute.For<IAppointmentRepository>();
			var items = new List<IAppointmentMessage> {_fixture.CreateAnonymous<AppointmentMessage>()};
			repo.GetEnumerator().Returns(x => items.GetEnumerator());

			return repo;
		}

		private void SetupController(ApiController controller) {
			var request = new HttpRequestMessage(HttpMethod.Post, ResourceUri);

			controller.Request = request;
			controller.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = _config;
		}
	}
}
