﻿using System;
using System.Threading;
using System.Threading.Tasks;

using NUnit.Framework;

namespace Escape.Studies.Manager.Tests {
	[TestFixture]
	public class BlockingQueueTests {
		private BlockingQueue<int> _sut;

		[SetUp]
		public void Setup() {
			_sut = new BlockingQueue<int>(1);
		}

		[Test]
		[ExpectedException(typeof(TaskCanceledException))]
		public async Task Dequeue_EmptyQueue_Blocks() {
			var cts = new CancellationTokenSource(250);
			await _sut.DequeueAsync(cts.Token);
		}

		[Test]
		[ExpectedException(ExpectedException = typeof(InvalidOperationException), ExpectedMessage = "Queue is full")]
		public void Enqueue_FullQueue_Throws_InvalidOperationException() {
			_sut.Enqueue(0);
			_sut.Enqueue(0);
		}

		[TestCase(new[] {2, 3, 5, 7, 11})]
		public async Task Dequeue_Returns_ValuesInOrder(int[] values) {
			_sut = new BlockingQueue<int>(values.Length);
			foreach (var i in values) {
				_sut.Enqueue(i);
			}

			var results = new int[values.Length];
			for (var i = 0; i < values.Length; i++) {
				results[i] = await _sut.DequeueAsync(CancellationToken.None);
			}

			CollectionAssert.AreEqual(values, results);
		}
	}
}
