﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using Escape.Data;

using Autofac;
using Autofac.Integration.WebApi;
using NUnit.Framework;
using Ploeh.AutoFixture;

namespace Escape.Studies.Manager.Tests
{
	[TestFixture]
	public class AppointmentControllerTests : IDisposable {
		private HttpServer _server;
		private AppointmentRepository _repo;
		private const string BaseUri = @"http://random-non-existant-domain.net";
		private const string AppUri = BaseUri + "/Appointment";
		private static readonly IFixture Fixture = new Fixture();

		[TestFixtureSetUp]
		public void FixtureSetup() {
			var cfg = new Manager().Config;
			_repo = new AppointmentRepository();

			// Set up IoC
			var builder = new ContainerBuilder();
			builder.RegisterApiControllers(typeof(Controllers.AppointmentController).Assembly);
			builder.Register(ctx => _repo)
				.As<IAppointmentRepository>()
				.SingleInstance();

			cfg.DependencyResolver = new AutofacWebApiDependencyResolver(builder.Build());
			cfg.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
			_server = new HttpServer(cfg);
		}

		[TestFixtureTearDown]
		public void Dispose() {
			if (_server != null) _server.Dispose();
		}

		[SetUp]
		public void Setup() {
			_repo.Clear();
		}

		[Test]
		public void HelloController_Returns_OK() {
			// Workaround for assembly loading issues
			// var type = typeof (Controllers.HelloController);
			var client = new HttpClient(_server);

			using (var resp = client.GetAsync(BaseUri + "/Hello").Result) {
				Assert.AreEqual(HttpStatusCode.OK, resp.StatusCode);
			}
		}

		[Test]
		public void Post_AppointmentRequest_ReturnsCreated() {
			var client = new HttpClient(_server);
			var stub = Fixture.CreateAnonymous<AppointmentRequest>();
			var resp = client.PostAsXmlAsync(AppUri, stub).Result;

			if (!resp.IsSuccessStatusCode) {
				var error = resp.Content.ReadAsAsync<HttpError>().Result;
				foreach (var kvp in error.Values) {
					Debug.WriteLine(kvp.ToString());
				}
			}

			Assert.AreEqual(HttpStatusCode.Created, resp.StatusCode);
		}

		[Test]
		public void Post_ConsecutiveAppointmentRequest_ReturnsSameLocation() {
			var client = new HttpClient(_server);
			var stub = Fixture.CreateAnonymous<AppointmentRequest>();

			var resp = client.PostAsXmlAsync(AppUri, stub).Result;
			var resp2 = client.PostAsXmlAsync(AppUri, stub).Result;

			if (!(resp.IsSuccessStatusCode && resp2.IsSuccessStatusCode)) {
				Debug.WriteLine("First response:");
				var error = resp.Content.ReadAsAsync<HttpError>().Result;
				foreach (var kvp in error.Values) {
					Debug.WriteLine(kvp.ToString());
				}

				Debug.WriteLine("Second response:");
				var error2 = resp2.Content.ReadAsAsync<HttpError>().Result;
				foreach (var kvp in error2.Values) {
					Debug.WriteLine(kvp.ToString());
				}
			}

			Assert.AreEqual(resp.Headers.Location, resp2.Headers.Location);
		}

		[Test]
		public void Post_TwoEqualAppointmentRequest_RepositoryCountRemainsConstant() {
			var client = new HttpClient(_server);
			var stub = Fixture.CreateAnonymous<AppointmentRequest>();

			var resp = client.PostAsXmlAsync(AppUri, stub).Result;
			var resp2 = client.PostAsXmlAsync(AppUri, stub).Result;

			Assert.AreEqual(1, _repo.Count);
		}

		[Test]
		public void Get_AfterPost_ReturnEqualAppointment() {
			var client = new HttpClient(_server);
			var stub = Fixture.CreateAnonymous<AppointmentRequest>();

			var loc = client.PostAsXmlAsync(AppUri, stub).Result.Headers.Location;
			var resp = client.GetAsync(loc).Result;
			var result = resp.Content.ReadAsAsync<AppointmentMessage>().Result;

			Assert.AreEqual(stub, result.Appointment);
		}
	}
}
