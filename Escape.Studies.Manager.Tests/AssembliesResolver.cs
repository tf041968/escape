﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dispatcher;
using Escape.Studies.Manager.Controllers;

namespace Escape.Studies.Manager.IntegrationTests {
	public class AssembliesResolver : IAssembliesResolver {
		public ICollection<Assembly> GetAssemblies() {
			// var baseAssemblies = base.GetAssemblies();
			var baseAssemblies = AppDomain.CurrentDomain.GetAssemblies();
			var assemblies = new List<Assembly>(baseAssemblies);
			assemblies.Add(Assembly.LoadFrom(typeof(AppointmentController).Assembly.CodeBase));

			return assemblies;
		}
	}
}
